Secuenciador:

Este programa tiene el objeto de ser un ejemplo ilustrativo de la potencialidad de la herramienta educativa "Scratch", diseñada por el MIT, orientada a niños con edad de nivel escolar primario (aproximadamente de entre 8 y 10 años de edad).
El mismo consiste en un secuenciador musical monofónico muy simple, en el que se pueden definir partituras personalizadas mediante listas de Scratch.
El término "monofónico", significa que este secuenciador, solo puede emitir una nota musical o sonido en un determinado instante de tiempo.
No podremos entonces, con este programa, reproducir una partitura compleja, en la que se superponen muchos sonidos en un determinado instante de tiempo.

Si bien la funcionalidad de este programa es muy limitada, es decir, se encuentra lejos de satisfacer las necesidades de un músico profesional, para el propósito educativo para el que fue diseñada resulta óptimo, ya que permitiría a los estudiantes que se encuentran en la etapa descripta, comenzar a desarrollar un pensamiento computacional básico de una manera visual y concreta, correspondiente al paradigma de la programación estructurada.

La complejidad de este programa, requiere de un cierto conocimiento previo para su comprensión. Es decir, no es recomendable explicar directamente la lógica de su funcionamiento a un estudiante sin ningún conocimiento previo en programación o computación. Si es factible, sin embargo, mostrarle a un estudiante con dichas caracteristicas el programa en ejecución y funcionando, de manera de que el mismo pueda observar el tipo de trabajos que podría hacer con la herramienta Scratch.
Luego de cierto período en que el estudiante absorba algunos conocimientos mas elementales, si sería conveniente entonces, explicarle la lógica de funcionamiento de este programa.

Las estructuras y elementos que utiliza este programa, con las que deseamos que el estudiante comienze a familiarizarse, son las siguientes:

- Variables de tipos elementales (números y cadenas de texto)
- Secuencias (listas) de elementos también de tipos elementales
- Operaciones condicionales
- Bucles
- Utilización de bloques personalizados (modularización)


Estamos aquí publicando la primera versión. Con el transcurso del tiempo, podremos ir añadiendo funcionalidades o modificando el código, de acuerdo a los requerimientos didácticos que resulten lo mas óptimos para los estudiantes.



Forma de utilización:

Este programa utiliza la extensión "Música" de Scratch. En ella, tenemos un número entero asociado a cada nota musical, y un bloque de código que nos permite reproducir esa nota durante un lapso determinado de tiempo. Este lapso de tiempo es absoluto (definido en relación al tempo establecido).

En este programa, una partitura consta de dos listas, las cuales deben tener la misma cantidad de elementos.
Una de esas listas corresponde a las notas de la partitura, y otra a las figuras musicales a cada una de esas notas.

Si la cantidad de elementos de la lista de notas, no se corresponde con la cantidad de elementos de la lista de figuras asociada, obtendremos resultados inesperados.

El proyecto trae dos partituras de ejemplo:
- Satisfaction (Rolling Stones)
- Breaking the law (Judas Priest)

Estas dos piezas fueron elegidas ya que cuentan con un patrón musical que se repite en el tiempo numerosas veces, y a la vez, dicho patrón es monofónico (no reproduce la canción completa, solo su patrón musical caracteristico).

La definición de tempo en Scratch, se refiere al tiempo en el que subdivide un minuto. Por defecto, su valor es 60, es decir que cada tiempo (beat) dura 1 segundo.
Si bien es posible modificarlo, a efectos de este trabajo lo dejamos en su valor por defecto, y asumimos que siempre, un tiempo equivale a un segundo.

A pesar de que la extensión "Música" nos permite establecer el tempo deseado, no nos permite indicar directamente las figuras musicales asociadas a una nota, al ser la misma reproducida. Por ejemplo, en una canción con un tempo de 120, establecemos que entran 120 negras en un minuto.
En ese caso entonces, una nota con figura negra suena durante 0,5 segundos.

Las figuras representan tiempo relativo, no absoluto. La duración en tiempo absoluto (en segundos), de una nota con una determinada figura musical, se define en relación al tempo.

Por lo tanto, debemos implementar nosotros un sistema que reconozca cada figura musical que definimos en la partitura.

Definimos hasta ahora las figuras a través de caracteres, de la siguiente forma:
- R : Redonda (Equivale a 4 negras)
- B : Blanca (Equivale a 2 negras)
- N : Negra
- Np : Negra con puntillo (Equivale a 1,5 negras)
- C : Corchea (Equivale a 1/2 negra)
- SC : Semicorchea (Equivale a 1/4 negra)

(Faltan implementar mas figuras, pudiendo tener una gran cantidad)


Tendremos que definir entonces, un bloque que nos calcule la duración en tiempo absoluto de una nota, en relación al tempo que asignamos y a la figura correspondiente.

Como cuestión importante: El tempo indicado que le pasaremos a dicho bloque NO es el tempo de la extensión "Música". Como dijimos antes, a este último lo dejamos en su valor por defecto (1 tiempo = 1 segundo), de manera que nos permita calcular mas fácilmente la duración absoluta de una nota con una determinada figura.

Si por ejemplo, tengo una figura negra y un tempo de 120.
El cálculo que utilizamos para establecer la duración de esa nota será: 60/120 = 0,5
Por lo tanto, en este caso la duración de esa nota será de 0,5 segundos.

Luego, tengo una figura corchea y un tempo también de 120.
El cálculo que utilizamos para establecer la duración de esa nota será: 30/120 = 0,25
Por lo tanto, en este caso la duración de esa nota será de 0,25 segundos.

Podría tener, una figura negra y un tempo de 150.
El cálculo que utilizamos para establecer la duración de esa nota será: 60/150 = 0,4
Por lo tanto, en este caso la duración de esa nota será de 0,4 segundos.


Definida esta función, el flujo principal del programa funciona de la siguiente manera:
-Tengo 4 variables: 'i', 'nota', 'figura' y 'duracion'

Al iniciar el programa (con la bandera verde), primero asigna el instrumento que selecciono. Puede ser el que mas me guste.
Luego, entra en el bucle principal, que dice 'repetir'. Aquí elegimos la cantidad de veces que queremos que se repita el patron musical.

Dentro del bucle principal, le damos el valor 1 a la variable 'i'.
Esta variable será nuestro índice: tiene el número del elemento de la partitura (con sus correspondientes listas), donde estamos parados.

Luego de hacer esto, colocamos un bucle secundario dentro del principal.

En este caso, no le decimos que repita una cantidad fija de veces, sinó tantas veces como notas tenga la partitura.
Para esto, dentro de 'repetir' no colocamos un número, sinó un bloque.
Este bloque será 'longitud de', y tendrá un menú desplegable. En este menú, seleccionamos la lista de notas de la partitura que queremos.

Podríamos también, en cambio, seleccionar la lista de figuras de la partitura que queremos.
Recordemos que en este programa, las listas de notas y de figuras de una partitura, deben tener la misma cantidad de elementos.


A continuación, dentro del bucle secundario, hacemos lo siguiente:


Seleccionamos la lista de notas correspondiente a la partitura que yo elija.

En ese mismo bloque, le asignamos a la variable 'nota', el valor del elemento de la lista de notas en el que estoy parado, con la variable 'i'.
Cuando por ejemplo, 'i' vale 1, y el valor de la primera nota que tenemos en la lista es '59', 'notas' ahora valdrá 59.

Luego, le damos a la variable 'figuras', el valor del elemento de la lista de figuras en el que estoy parado con la variable 'i'.
Cuando por ejemplo, 'i' vale 1, y el valor de la primera figura que tenemos en la lista es 'N', 'figuras' ahora valdrá 'N'.

En el siguiente bloque, llamamos a nuestro propio bloque definido para calcular la duración absoluta de la nota a reproducir.
Le indicamos también, el tempo en el que queremos que la partitura se reproduzca.

Este bloque, va a calcular la duración absoluta (en segundos) que debe durar dicha nota, en base al tempo y la figura.
Me va a guardar el resultado en la variable 'duración'.

A continuación, siguiendo en el bucle secundario, reproduce la nota que tenemos en la variable 'nota', con la duración que tenemos en la variable 'duracion'.

Finalmente, le suma 1 a la variable 'i'. Si antes en 'i' teníamos un 1, ahora tendremos un 2. Nos deja parados entonces, en el siguiente elemento de la partitura.

Se repite entonces, el bucle secundario, tantas veces hasta que haya reproducido cada nota de la partitura, con su correspondiente duración.

Luego, al volver al bucle principal, ejecutamos el bucle secundario nuevamente de acuerdo a la cantidad de veces que yo le indiqué que se repita.
Vemos que allí, la variable 'i' vuelve a ser 1, de forma que la utilizo para recorrer toda la partitura nuevamente.

De esta forma, volveremos a escuchar la misma melodía una y otra vez, tantas veces como querramos (hasta que nos cansemos).

¡Animemosnos entonces a crear nuestras propias partituras!
